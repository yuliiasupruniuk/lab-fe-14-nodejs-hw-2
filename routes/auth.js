const Router = require('express');
const router = new Router();

const AuthController = require('../controllers/authController');
const logMiddleware = require('../middleware/logMiddleware');

router.post('/register', logMiddleware, AuthController.register);
router.post('/login', logMiddleware, AuthController.login);

module.exports = router;
