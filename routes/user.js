const Router = require('express');
const router = new Router();

const UserController = require('../controllers/userController');
const authMiddleware = require('../middleware/authMiddleware');
const logMiddleware = require('../middleware/logMiddleware');

router.get('/', [authMiddleware, logMiddleware], UserController.getUserProfile);

router.patch(
    '/',
    [authMiddleware, logMiddleware],
    UserController.updateUserProfile,
);

router.delete(
    '/',
    [authMiddleware, logMiddleware],
    UserController.deleteUserProfile,
);

module.exports = router;
