const Router = require('express');
const router = new Router();

const authRouter = require('./auth');
const noteRouter = require('./notes');
const userRouter = require('./user');

router.use('/auth', authRouter);
router.use('/notes', noteRouter);
router.use('/users/me', userRouter);

module.exports = router;
