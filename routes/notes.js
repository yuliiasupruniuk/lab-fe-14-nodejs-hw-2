const Router = require('express');
const router = new Router();

const NoteController = require('../controllers/noteController');
const authMiddleware = require('../middleware/authMiddleware');
const logMiddleware = require('../middleware/logMiddleware');

router.get('/', [authMiddleware, logMiddleware], NoteController.getAllNotes);
router.get('/:id', [authMiddleware, logMiddleware], NoteController.getNoteById);

router.post('/', [authMiddleware, logMiddleware], NoteController.createNote);
router.put('/:id', [authMiddleware, logMiddleware], NoteController.updateNote);
router.patch('/:id', [authMiddleware, logMiddleware], NoteController.checkNote);

router.delete(
    '/:id',
    [authMiddleware, logMiddleware],
    NoteController.deleteNote,
);

module.exports = router;
