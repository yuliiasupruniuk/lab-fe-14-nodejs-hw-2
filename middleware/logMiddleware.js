/**
 * @param {object} req - request
 */
function getRequestLog(req) {
  const currentDatetime = new Date();
  const formattedDate =
    currentDatetime.getFullYear() +
    '-' +
    (currentDatetime.getMonth() + 1) +
    '-' +
    currentDatetime.getDate() +
    ' ' +
    currentDatetime.getHours() +
    ':' +
    currentDatetime.getMinutes() +
    ':' +
    currentDatetime.getSeconds();
  const reqMethod = req.method;
  const reqURL = req.protocol + '://' + req.get('host') + req.originalUrl;
  const log = `[${formattedDate}] ${reqMethod} ${reqURL}`;
  console.log(log);
}

/**
 * @param {object} res - response
 */
function getResponseLog(res) {
  const oldWrite = res.write;
  const oldEnd = res.end;
  const chunks = [];

  res.write = function(chunk) {
    chunks.push(chunk);

    return oldWrite.apply(res, [chunk]);
  };

  res.end = function(chunk) {
    if (chunk) chunks.push(chunk);

    const body = Buffer.concat(chunks).toString('utf8');
    const log = `Status ${res.statusCode} ${body}`;
    console.log(log);
    oldEnd.apply(res, [chunk]);
  };
}

module.exports = async function(req, res, next) {
  getRequestLog(req);
  getResponseLog(res);

  next();
};
