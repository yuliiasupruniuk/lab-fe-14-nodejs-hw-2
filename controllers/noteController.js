const mongoose = require('mongoose');
const ApiError = require(`../errors/apiError`);
const Note = require(`../models/note`);

/**
 * Notes handler
 */
class NoteController {
  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async getAllNotes(req, res, next) {
    let {limit, offset} = req.query;
    offset = +offset || 0;
    limit = +limit || 0;
    const userId = req.user.id;
    let notes = [];

    try {
      if (!limit) {
        notes = await Note.find({userId}, {updatedAt: 0, __v: 0}).skip(
            offset);
      } else {
        notes = await Note.find({userId}, {updatedAt: 0, __v: 0})
            .skip(offset)
            .limit(limit);
      }

      const count = await Note.countDocuments({userId});

      res.json({offset, limit, count, notes});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async getNoteById(req, res, next) {
    const noteId = req.params.id;
    const userId = req.user.id;

    if (!mongoose.isValidObjectId(noteId)) {
      return next(ApiError.badRequest(`Invalid note id`));
    }

    try {
      const note = await Note.findOne({_id: noteId}, {updatedAt: 0, __v: 0});
      if (!note) {
        return next(ApiError.badRequest(`Note with id - ${noteId} not found`));
      }

      if (`${note.userId}` !== userId) {
        return next(
            ApiError.badRequest(
                `You don't have access to note with id - ${noteId}`));
      }
      res.json({note});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async createNote(req, res, next) {
    const {text} = req.body;
    const _id = req.user.id;

    if (!text) {
      return next(ApiError.badRequest(`Field 'text' is required`));
    }

    try {
      const newNote = new Note({text, userId: _id});
      await newNote.save();

      res.json({
        message: `Success`,
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async updateNote(req, res, next) {
    const userId = req.user.id;
    const noteId = req.params.id;
    const {text} = req.body;

    if (!text) {
      return next(ApiError.badRequest(`Field 'text' is required`));
    }

    if (!mongoose.isValidObjectId(noteId)) {
      return next(ApiError.badRequest(`Invalid note id`));
    }

    try {
      const note = await Note.findOne({_id: noteId}, {updatedAt: 0, __v: 0});
      if (!note) {
        return next(ApiError.badRequest(`Note with id - ${noteId} not found`));
      }

      if (`${note.userId}` !== userId) {
        return next(
            ApiError.badRequest(
                `You don't have access to note with id - ${noteId}`));
      }

      await Note.findOneAndUpdate({_id: noteId}, {text});

      res.json({
        message: `Success`,
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async checkNote(req, res, next) {
    const userId = req.user.id;
    const noteId = req.params.id;

    if (!mongoose.isValidObjectId(noteId)) {
      return next(ApiError.badRequest(`Invalid note id`));
    }

    try {
      const note = await Note.findOne({_id: noteId}, {updatedAt: 0, __v: 0});
      if (!note) {
        return next(ApiError.badRequest(`Note with id - ${noteId} not found`));
      }

      if (`${note.userId}` !== userId) {
        return next(
            ApiError.badRequest(
                `You don't have access to note with id - ${noteId}`));
      }

      await Note.findOneAndUpdate({_id: noteId}, {completed: !note.completed});

      res.json({
        message: `Success`,
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async deleteNote(req, res, next) {
    const userId = req.user.id;
    const noteId = req.params.id;

    if (!mongoose.isValidObjectId(noteId)) {
      return next(ApiError.badRequest(`Invalid note id`));
    }

    try {
      const note = await Note.findOne({_id: noteId}, {updatedAt: 0, __v: 0});
      if (!note) {
        return next(ApiError.badRequest(`Note with id - ${noteId} not found`));
      }

      if (`${note.userId}` !== userId) {
        return next(
            ApiError.badRequest(
                `You don't have access to note with id - ${noteId}`));
      }

      await Note.deleteOne({_id: noteId});

      res.json({
        message: `Success`,
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }


  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async deleteUserNotes(req, res, next) {
    const userId = req.user.id;

    try {
      await Note.deleteMany({userId});

      res.json({
        message: 'Success',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new NoteController();
