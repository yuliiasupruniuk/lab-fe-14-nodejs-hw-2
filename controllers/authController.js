const ApiError = require('../errors/apiError');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_KEY = 'viburnum';

const generateToken = (id, username, createdDate) => {
  return jwt.sign(
      {
        id,
        username,
        createdDate,
      },
      JWT_KEY,
      {expiresIn: '24h'},
  );
};

/**
 * Authorization data handler
*/
class AuthController {
  /**
   * User registration
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async register(req, res, next) {
    const {username, password} = req.body;

    if (!username) {
      return next(ApiError.badRequest(`Field 'username' is required`));
    } else if (!password) {
      return next(ApiError.badRequest(`Field 'password' is required`));
    }

    try {
      const checkUser = await User.findOne({username: username});

      if (checkUser) {
        return next(ApiError.badRequest(`User already exists`));
      }

      const passwordHash = await bcrypt.hash(password, 7);

      const user = new User({username, password: passwordHash, notes: []});
      await user.save();

      res.json({
        message: 'Success',
      });
    } catch (e) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * User sign in
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async login(req, res, next) {
    const {username, password} = req.body;

    if (!username) {
      return next(ApiError.badRequest(`Field 'username' is required`));
    } else if (!password) {
      return next(ApiError.badRequest(`Field 'password' is required`));
    }

    try {
      const user = await User.findOne({username: username});
      if (!user) {
        return next(ApiError.badRequest('User not found'));
      }

      const comparePassword = await bcrypt.compare(password, user.password);
      if (!comparePassword) {
        return next(ApiError.badRequest('Wrong password'));
      }

      const token = generateToken(user._id, user.username, user.createdDate);
      return res.json({message: 'Success', jwt_token: token});
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new AuthController();
