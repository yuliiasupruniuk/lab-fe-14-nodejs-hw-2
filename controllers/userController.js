const ApiError = require('../errors/apiError');
const NoteController = require('../controllers/noteController');
const User = require('../models/user');
const bcrypt = require('bcrypt');

/**
 * User profile handler
 */
class UserController {
  /**
   * User profile view
   * @param {object} req - request
   * @param {object} res - response
   */
  async getUserProfile(req, res) {
    const {id, username, createdDate} = req.user;

    res.json({
      user: {
        _id: id,
        username,
        createdDate,
      },
    });
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async updateUserProfile(req, res, next) {
    const {oldPassword, newPassword} = req.body;
    const _id = req.user.id;

    if (!oldPassword) {
      return next(ApiError.badRequest(`Field 'oldPassword' is required`));
    } else if (!newPassword) {
      return next(ApiError.badRequest(`Field 'newPassword' is required`));
    }

    try {
      const user = await User.findOne({_id});

      const comparePassword = await bcrypt.compare(oldPassword, user.password);
      if (!comparePassword) {
        return next(ApiError.badRequest('Wrong password'));
      }

      const passwordHash = await bcrypt.hash(newPassword, 7);
      await User.findOneAndUpdate({_id}, {password: passwordHash});

      res.json({
        message: 'Success',
      });
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }

  /**
   * @param {object} req - request
   * @param {object} res - response
   * @param {function} next
   */
  async deleteUserProfile(req, res, next) {
    const userId = req.user.id;

    try {
      await User.deleteOne({_id: userId});

      NoteController.deleteUserNotes(req, res, next);
    } catch (err) {
      return next(ApiError.internal(`Server error`));
    }
  }
}

module.exports = new UserController();
