const {Schema, model} = require('mongoose');

const note = new Schema(
    {
      userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
      },
      completed: {
        type: Boolean,
        required: true,
        default: false,
      },
      text: {
        type: String,
        required: true,
      },
    },
    {timestamps: {createdAt: 'createdDate'}},
);

module.exports = model('Note', note);
